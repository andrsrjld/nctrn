import React from "react";

class Home extends React.Component {
  render() {
    return (
      <div id="home" className="section full-width-section" data-component="home">
          <div className="section-wrapper block">
              <div className="home-left-part">
                  <p className="site-des">Hello. My name is</p>
                  <h1 className="entry-title">Christian Rizaldi</h1>                                
                  <p className="site-info">
                      I'm a programmer and also an allround web developer with good knowledge of front-end techniques. I like to structure and order and also stand for quality. I also spending time for fixing little details and optimizing web apps. moreover than that I would to work as a team, you'll learn faster and much more. As the saying goes: 'two heads are better than one'.
                  </p>

                  <div className="social-links">
                      <a target="blank" href="https://twitter.com/andrs_rjld">TWITTER</a>
                      <a target="blank" href="https://www.instagram.com/andrs_rjld/">INSTAGRAM</a>
                  </div>

              </div>                              
              <div className="home-right-part">                                
              </div>                            
          </div>
      </div>
    );
  }
}

export default Home;
