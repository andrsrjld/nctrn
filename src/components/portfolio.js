import React from "react";

class Portfolio extends React.Component {
  render() {
    return (
      <div id="portfolio" classNameName="section">
        <div classNameName="section-wrapper block">
          <div classNameName="content-1300">
            <div id="portfolio-wrapper" classNameName="relative">
              <div classNameName="category-filter">
                <div classNameName="category-filter-icon" />
              </div>
              <div classNameName="category-filter-list button-group filters-button-group">
                <div classNameName="button is-checked" data-filter="*">
                  All
                </div>
                <div classNameName="button" data-filter=".text">
                  Website
                </div>
                <div classNameName="button" data-filter=".video">
                  Video
                </div>
                <div classNameName="button" data-filter=".image">
                  Image
                </div>
              </div>
              <div classNameName="portfolio-load-content-holder" />
              <div classNameName="grid" id="portfolio-grid">
                <div classNameName="grid-sizer" />
                <div
                  id="p-item-1"
                  classNameName="grid-item element-item p-one-third text"
                >
                  <a
                    classNameName="item-link ajax-portfolio"
                    href="portfolio-4.html"
                    data-id={7}
                  >
                    <img src="images/ardanradio.png" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">Ardanradio</p>
                        <p classNameName="portfolio-cat">Website</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div
                  id="p-item-1"
                  classNameName="grid-item element-item p-one-third text"
                >
                  <a
                    classNameName="item-link ajax-portfolio"
                    href="portfolio-5.html"
                    data-id={6}
                  >
                    <img src="images/Unpas.png" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">Fisip Unpas</p>
                        <p classNameName="portfolio-cat">Website</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div
                  id="p-item-1"
                  classNameName="grid-item element-item p-one-third text"
                >
                  <a
                    classNameName="item-link ajax-portfolio"
                    href="portfolio-6.html"
                    data-id={5}
                  >
                    <img src="images/Exsport.png" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">Exsport</p>
                        <p classNameName="portfolio-cat">Website</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div
                  id="p-item-4"
                  classNameName="grid-item element-item p-one-third text"
                >
                  <a
                    classNameName="item-link ajax-portfolio"
                    href="portfolio-7.html"
                    data-id={4}
                  >
                    <img src="images/Bodypack.png" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">Bodypack</p>
                        <p classNameName="portfolio-cat">Website</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div
                  id="p-item-1"
                  classNameName="grid-item element-item p-one-third text"
                >
                  <a
                    classNameName="item-link ajax-portfolio"
                    href="portfolio-1.html"
                    data-id={1}
                  >
                    <img src="images/orca.jpg" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">Orca Racing</p>
                        <p classNameName="portfolio-cat">Website</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div
                  id="p-item-2"
                  classNameName="grid-item element-item p-one-third text"
                >
                  <a
                    classNameName="item-link ajax-portfolio"
                    href="portfolio-2.html"
                    data-id={2}
                  >
                    <img src="images/eiger.jpg" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">Eigerindo</p>
                        <p classNameName="portfolio-cat">Website</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div
                  id="p-item-3"
                  classNameName="grid-item element-item p-one-third text"
                >
                  <a
                    classNameName="item-link ajax-portfolio"
                    href="portfolio-3.html"
                    data-id={3}
                  >
                    <img src="images/jonas.jpg" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">Jonas Photo</p>
                        <p classNameName="portfolio-cat">Website</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div classNameName="grid-item element-item p-one-third video">
                  <a
                    classNameName="item-link"
                    href="https://youtu.be/hu6AfJgGKb0"
                    data-rel="prettyPhoto[portfolio]"
                  >
                    <img src="images/hqdefault3.jpg" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">
                          Diginet Media Tour 2017
                        </p>
                        <p classNameName="portfolio-cat">Video</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div classNameName="grid-item element-item p-one-third video">
                  <a
                    classNameName="item-link"
                    href="https://youtu.be/qHkn-bkSu-8"
                    data-rel="prettyPhoto[portfolio]"
                  >
                    <img src="images/hqdefault2.jpg" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">Catatan Akhir Sekolah</p>
                        <p classNameName="portfolio-cat">Video</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div classNameName="grid-item element-item p-one-third video">
                  <a
                    classNameName="item-link"
                    href="https://youtu.be/2t5h_sJ_2ro"
                    data-rel="prettyPhoto[portfolio]"
                  >
                    <img src="images/hqdefault.jpg" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">
                          Arti Sahabat short movie
                        </p>
                        <p classNameName="portfolio-cat">Video</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div classNameName="grid-item element-item p-one-third video">
                  <a
                    classNameName="item-link"
                    href="https://www.youtube.com/watch?v=1y_DASPrB9w"
                    data-rel="prettyPhoto[portfolio]"
                  >
                    <img src="images/safe and sound.png" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">
                          Safe And Sound Cover by Jaen Richa
                        </p>
                        <p classNameName="portfolio-cat">Video</p>
                      </div>
                    </div>
                  </a>
                </div>
                <div classNameName="grid-item element-item p-one-third video">
                  <a
                    classNameName="item-link"
                    href="https://www.youtube.com/watch?v=tV4cOlpSUNE&t=233s"
                    data-rel="prettyPhoto[portfolio]"
                  >
                    <img src="images/buku.png" alt />
                    <div classNameName="portfolio-text-holder">
                      <div classNameName="portfolio-text-wrapper">
                        <p classNameName="portfolio-text">BUKU (Short Movie)</p>
                        <p classNameName="portfolio-cat">Video</p>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Portfolio;
