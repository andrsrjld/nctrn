import React from "react";

class contact extends React.Component {
  render() {
    return (
      <div id="contact" classNameName="section">
        <div classNameName="section-wrapper block">
          <div classNameName="content-1300">
            <div classNameName="row">
              <h2 classNameName="entry-title section-title">Get in touch</h2>
              <div classNameName="one-half width-40">
                <p classNameName="section-info">
                  We think that the continuous contacts gave the professional
                  relationship a personal touch, and it is reasonable to believe
                  that this relationship helped to facilitate the spouses'
                  grieving-process.
                </p>
                <p>
                  <b>ADDRESS:</b> Hassanudin St. 741d Salatiga <br />
                  <b>EMAIL:</b> andreasallbuqori@gmail.com <br />
                  <b>WEBSITE:</b> www.wit.id <br />
                </p>
              </div>
              <div classNameName="one-half width-55 last">
                <div classNameName="contact-form">
                  <form
                    action="mailto:andreasallbuqori@gmail.com"
                    method="post"
                    encType="text/plain"
                  >
                    <p>
                      <input
                        id="name"
                        type="text"
                        name="your-name"
                        placeholder="Name"
                      />
                    </p>
                    <p>
                      <input
                        id="contact-email"
                        type="email"
                        name="your-email"
                        placeholder="Email"
                      />
                    </p>
                    <p>
                      <input
                        id="subject"
                        type="text"
                        name="your-subject"
                        placeholder="Subject"
                      />
                    </p>
                    <p>
                      <textarea
                        id="message"
                        name="your-message"
                        placeholder="Message"
                        defaultValue={""}
                      />
                    </p>
                    <input target="blank" type="submit" />
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default contact;
