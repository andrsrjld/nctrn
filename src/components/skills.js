import React from "react";

class skills extends React.Component {
  render() {
    return (
      <div id="skills" classNameName="section">
        <div classNameName="section-wrapper block">
          <div classNameName="content-1300">
            <div classNameName="row m-bottom-60">
              <h2 classNameName="entry-title section-title">Skills</h2>
              <div classNameName="skill-circle-holder">
                <div classNameName="skill-circle">
                  <div
                    classNameName="skill-circle-wrapper relative"
                    data-value="0.82"
                    data-color="#90A1A1"
                    data-empty-color="#554247"
                  >
                    <span classNameName="skill-circle-num" />
                  </div>
                  <p classNameName="skill-circle-text">Photoshop</p>
                </div>
                <div classNameName="skill-circle">
                  <div
                    classNameName="skill-circle-wrapper relative"
                    data-value="0.6"
                    data-color="#90A1A1"
                    data-empty-color="#554247"
                  >
                    <span classNameName="skill-circle-num" />
                  </div>
                  <p classNameName="skill-circle-text">Web Developer</p>
                </div>
                <div classNameName="skill-circle">
                  <div
                    classNameName="skill-circle-wrapper relative"
                    data-value="0.75"
                    data-color="#90A1A1"
                    data-empty-color="#554247"
                  >
                    <span classNameName="skill-circle-num" />
                  </div>
                  <p classNameName="skill-circle-text">CorelDraw</p>
                </div>
                <div classNameName="skill-circle">
                  <div
                    classNameName="skill-circle-wrapper relative"
                    data-value="0.31"
                    data-color="#90A1A1"
                    data-empty-color="#554247"
                  >
                    <span classNameName="skill-circle-num" />
                  </div>
                  <p classNameName="skill-circle-text">Photography</p>
                </div>
              </div>
            </div>
            <div classNameName="row">
              <div classNameName="one-half">
                <div classNameName="skills-holder">
                  <div classNameName="skill-holder">
                    <div classNameName="skill-text">
                      <div classNameName="skill">
                        <div classNameName="skill-fill" data-fill="75%" />
                      </div>
                      <span>JavaScript</span>
                    </div>
                    <div classNameName="skill-percent">75%</div>
                  </div>
                  <div classNameName="skill-holder">
                    <div classNameName="skill-text">
                      <div classNameName="skill">
                        <div classNameName="skill-fill" data-fill="48%" />
                      </div>
                      <span>PHP</span>
                    </div>
                    <div classNameName="skill-percent">48%</div>
                  </div>
                </div>
              </div>
              <div classNameName="one-half last">
                <div classNameName="skills-holder sec-skills-holder">
                  <div classNameName="skill-holder">
                    <div classNameName="skill-text">
                      <div classNameName="skill">
                        <div classNameName="skill-fill" data-fill="90%" />
                      </div>
                      <span>HTML/CSS</span>
                    </div>
                    <div classNameName="skill-percent">90%</div>
                  </div>
                  <div classNameName="skill-holder">
                    <div classNameName="skill-text">
                      <div classNameName="skill">
                        <div classNameName="skill-fill" data-fill="62%" />
                      </div>
                      <span>CMS</span>
                    </div>
                    <div classNameName="skill-percent">62%</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default skills;
