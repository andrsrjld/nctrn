import React from "react";

class resume extends React.Component {
  render() {
    return (
      <div id="resume" classNameName="section">
        <div classNameName="section-wrapper block">
          <div classNameName="content-1300">
            <div classNameName="row">
              <div classNameName="one-half width-55">
                <h2 classNameName="entry-title section-title">Experience</h2>
                <ul classNameName="timeline-holder">
                  <li classNameName="timeline-event">
                    <span classNameName="timeline-circle" />
                    <div classNameName="timeline-event-content">
                      My name is Christian Rizaldi. I was born in Salatiga June
                      28th 1999.
                    </div>
                    <div classNameName="timeline-event-date">1999</div>
                  </li>
                  <li classNameName="timeline-event">
                    <span classNameName="timeline-circle" />
                    <div classNameName="timeline-event-content">
                      <b>2006 - 2012</b> i started studying at Mangunsari 05
                      Elementary School salatiga for 6 years.
                    </div>
                    <div classNameName="timeline-event-date">2006</div>
                  </li>
                  <li classNameName="timeline-event">
                    <span classNameName="timeline-circle" />
                    <div classNameName="timeline-event-content">
                      <b>2012 - 2014</b> Ii started studying at Stella Matutina
                      Middle School salatiga for 3 years. I started to put my
                      interest on web developing. I started to make my first
                      project and got some experiences about programming.
                    </div>
                    <div classNameName="timeline-event-date">2012</div>
                  </li>
                  <li classNameName="timeline-event">
                    <span classNameName="timeline-circle" />
                    <div classNameName="timeline-event-content">
                      <b>2014 - 2017</b> i started studying at Saraswati High
                      School salatiga for 3 years. I passed the programming
                      development subject with almost perfect score.
                    </div>
                    <div classNameName="timeline-event-date">2014</div>
                  </li>
                  <li classNameName="timeline-event">
                    <span classNameName="timeline-circle" />
                    <div classNameName="timeline-event-content">
                      <b>2017 - 2019</b> Work as Web Developer especially on PHP
                      (CI, Yii, and Laravel) in goverment IT Consultant called
                      Diginet Media based on Special Region of Yogyakarta
                      Indonesia.
                    </div>
                    <div classNameName="timeline-event-date">2017</div>
                  </li>
                  <li classNameName="timeline-event">
                    <span classNameName="timeline-circle" />
                    <div classNameName="timeline-event-content">
                      <b>2019 - Present</b> Work as Web Developer especially on
                      PHP (Native) and CMS or eCommerce Project in IT
                      Consultanst called WIT.ID based on Bandung Indonesia.
                    </div>
                    <div classNameName="timeline-event-date">2019</div>
                  </li>
                </ul>
              </div>
              <div classNameName="one-half width-40 last">
                <h2 classNameName="entry-title section-title">Cover letter</h2>
                <p>
                  This letter is aimed to express my interest in the job posted
                  on your website for an experienced, detailed-oriented,
                  front-end web developer. As you'll see, I have 3 years of
                  hands-on experience efficiently coding websites and
                  applications using modern HTML, CSS, and JavaScript.
                </p>
                <p>
                  Building state-of-the-art, easy to use, user-friendly websites
                  and applications is truly a passion of mine and I am confident
                  I would be an excellent addition to your organization. In
                  addition to my knowledge base, I actively seek out new
                  technologies and stay up-to-date on industry trends and
                  advancements. This has allowed me to stay ahead of the curve
                  and deliver exceptional work to all of my employers, including
                  those I've worked for on a project basis. I’ve attached a copy
                  of my resume detailing my experience, along with links to
                  websites and applications I’ve had the honor of working on.
                </p>
                <p>
                  I can be reached anytime via my cell phone, 0812-2664-9552 or
                  by email at andreasallbuqori@gmail.com.
                </p>
                <p>
                  I look forward for having a conversation with you about this
                  opportunity.
                </p>
                <p>Best Regards.</p>
                <img classNameName="my-signature" src="images/signature2.png" alt />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default resume;
