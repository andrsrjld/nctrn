import React from "react";

class Service extends React.Component {
  render() {
    return (
      <div id="service" className="section" data-component="service">                        
      <div className="section-wrapper block">
          <div className="content-1300">
              <div className="row">
                  <div className="one-half width-55">
                      <div className="services-wrapper">
                          <div className="service-holder">
                              <img src="./images/service1.png"  />
                              <h4 className="service-title">Design what you want.</h4>
                              <div className="service-text">
                                  I like to keep it simple. My goals are to focus on typography, content and conveying the message that you want to send.
                              </div>                                        
                          </div>                               

                          <div className="service-holder m-right-0">
                              <img src="./images/service2.png"  />
                              <h4 className="service-title">Develop what you need.
                              </h4>
                              <div className="service-text">
                                  I'm a developer, so I know how to create your website to run across devices using the latest technologies available.
                              </div>                                        
                          </div>
                      </div>
                  </div>

                  <div className="one-half width-40 last">                                

                      <h2 className="entry-title section-title">
                          I’m currently available for freelance work.
                      </h2>
                      <p className="section-info" >
                          If you have a project that you want to get started, think you need my help with something or just fancy saying hey, then get in touch.
                      </p>

                      <div className="button-group-wrapper">
                          <a className="button" target="blank" href="https://drive.google.com/open?id=1NLyNVKPV6oMjUo_28zHyKAr1sb2tAeF8">Download CV</a> 
                          <a href="#portfolio" className="button">Check My Portfolio</a>                                                                   
                      </div>
                  </div>                                                                    
              </div>
          </div>
      </div>
  </div>
    );
  }
}

export default Service;
