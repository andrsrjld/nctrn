import React from 'react';
import './App.css';
import './style.css'
import './clear.css'
import './common.css'
import Header from './components/Header';
import Home from './components/Home';
import Service from './components/service';
import Portfolio from './components/portfolio';

function App() {
  return (
    <div className="App">
      
      <div id="content" className="site-content">
        <Header/>
        <div className="content-right">  
          <div className="content-right-wrapper">
            <Home/>
            {/* <Service/>
            <Portfolio/> */}
          </div>
        </div>
      </div>

        <script src="./js/jquery.js"></script>			                                                               
        <script src='./js/imagesloaded.pkgd.js'></script>                
        <script src='./js/jquery.prettyPhoto.js'></script>                
        <script src='./js/isotope.pkgd.js'></script>                
        <script src='./js/jquery.smartmenus.min.js'></script>                           
        <script src='./js/owl.carousel.min.js'></script>                                                            
        <script src='./js/circle-progress.min.js'></script>                                                            
        <script src='./js/main.js'></script>

    </div>
  );
}

export default App;
